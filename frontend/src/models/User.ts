import Avatar from "./Avatar";

export default class User {
    constructor(
        public avatar: Avatar){}
}