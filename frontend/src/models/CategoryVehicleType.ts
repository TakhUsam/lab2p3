import VehicleType from "./VehicleType";
import Category from "./Category";

export default class CategoryVehicleType {
  constructor(
    public id: number,
    public category: Category,
    public vehicle_type: VehicleType) { }
}
