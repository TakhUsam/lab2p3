import React, { useEffect, useState } from 'react';
import './SegmentToggle.css';
import { ApiUrl } from "../services/ApiService";
import { IonList, IonItem, IonRow, IonCol, IonButton } from '@ionic/react';

import './OffersList.css';
import Offer from '../models/Offer';
import Order from '../models/Order';


interface OffersList {
    idJob: number;
    onSelectClicked: (id: string) => void;
}


const OffersList: React.FC<OffersList> = ({ onSelectClicked, idJob }) => {

    const [offers, setOffers] = useState<Offer[]>([]);
    const [orders, setOrders] = useState<Order[]>([]);

    useEffect(() => {
        fetch(`${ApiUrl}orders/`).then(async response => {
          setOrders(await response.json());
        });
      }, []);

    useEffect(() => {
        fetch(`${ApiUrl}offers/`).then(async response => {
            setOffers(await response.json());
        });
    }, []);

    if (idJob==0)
        return (
            <IonList>
                {
                    //todo need added filter ID user
                    offers.filter(x => orders.filter(or=>or.offer.toString()==x.id).length>=1).map(offer => {
                        var of = offer;
                        return (
                            <IonItem className="shadow" key={offer.id}>
                                <IonRow>
                                    <IonCol size="3">
                                        <div className="container">
                                            <img src={offer.vehicle.driver.user.avatar.url} />
                                        </div>
                                    </IonCol>
                                    <IonCol size="9">
                                        <IonRow>
                                            <IonCol size="7">
                                                <p className="left">{offer.vehicle.model.type.name} </p> <p className="left rating"> &#9733;4.7</p>
                                            </IonCol>
                                            <IonCol size="5">
                                                <p className="right">{Math.trunc(offer.amount)} &#8381;</p>
                                            </IonCol>
                                            <IonCol size="6">
                                                <p className="left rplate rplate-left">{offer.vehicle.registration_plate.substr(0, 6)}</p>
                                                <p className="left rplate rplate-right">{offer.vehicle.registration_plate.substr(6, 3)}</p>
                                            </IonCol>
                                            <IonCol size="6">
                                                <p className="right">900m</p>
                                            </IonCol>
                                        </IonRow>
                                    </IonCol>

                                    <IonCol>
                        <IonButton color="primary" expand="block" className="place-button">Принято на {orders.find(x=>x.offer.toString()==offer.id)?.id} id</IonButton>
                                    </IonCol>
                                </IonRow>
                            </IonItem>
                        )
                    })
                }
            </IonList>
        );

    return (
        <IonList>
            {
                //todo need added filter ID user
                offers.filter(x => x.job == idJob).map(offer => {
                    var of = offer;
                    return (
                        <IonItem className="shadow" key={offer.id}>
                            <IonRow>
                                <IonCol size="3">
                                    <div className="container">
                                        <img src={offer.vehicle.driver.user.avatar.url} />
                                    </div>
                                </IonCol>
                                <IonCol size="9">
                                    <IonRow>
                                        <IonCol size="7">
                                            <p className="left">{offer.vehicle.model.type.name} </p> <p className="left rating"> &#9733;4.7</p>
                                        </IonCol>
                                        <IonCol size="5">
                                            <p className="right">{Math.trunc(offer.amount)} &#8381;</p>
                                        </IonCol>
                                        <IonCol size="6">
                                            <p className="left rplate rplate-left">{offer.vehicle.registration_plate.substr(0, 6)}</p>
                                            <p className="left rplate rplate-right">{offer.vehicle.registration_plate.substr(6, 3)}</p>
                                        </IonCol>
                                        <IonCol size="6">
                                            <p className="right">900m</p>
                                        </IonCol>
                                    </IonRow>
                                </IonCol>

                                <IonCol>
                                    <IonButton onClick={() => onSelectClicked(offer.id)} color="primary" expand="block" className="place-button">Принять</IonButton>
                                </IonCol>
                            </IonRow>
                        </IonItem>
                    )
                })
            }
        </IonList>
    );
};

export default OffersList;
