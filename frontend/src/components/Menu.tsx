import {
  IonContent,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonImg,
  IonBadge,
} from '@ionic/react';

import React from 'react';
import { useLocation } from 'react-router-dom';
import './Menu.css';



const appPages = [
  {
    title: 'Заказы',
    url: '/orders',
    countOrders:4
  },
  {
    title: 'Предложения',
    url: '/offers/0'
  },
  {
    title: 'Способы оплаты',
    imageUrl: "visa_PNG3.png"
  },
  {
    title: 'Настройки',
  },
  {
    title: 'Служба поддержки'
  },
  {
    title: 'Стать водителем',
    color: "tertiary"
  }
];


const Menu: React.FC = () => {
  const location = useLocation();

  return (
    <IonMenu contentId="main" type="overlay">
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader className="position-title">Тадаев Сайд-Хамзат</IonListHeader>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonLabel color={appPage.color}>{appPage.title}</IonLabel>
                  {
                    appPage.countOrders &&
                     <IonBadge color="danger">{appPage.countOrders}</IonBadge>
                  }

                  {
                    appPage.imageUrl &&
                    <div>
                      .1234
                      <IonImg className="icon" src={`../assets/icon/${appPage.imageUrl}`} />
                    </div>
                  }

                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
        <p className="exit">Выйти</p>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
