import React, { useEffect, useState } from 'react';
import { IonButton, IonCol, IonRow, IonContent, IonModal, IonItem, IonInput, IonText, IonDatetime } from '@ionic/react';
import './NewOrder.css';
import OrderReady from './OrderReady';
import { useHistory } from 'react-router';
import axios from 'axios';
import { ApiUrl } from "../services/ApiService";
import VehicleTypeSelector from "./VehicleTypeSelector";
import Address from '../models/Address';

export interface MapAddress {
  coords: number[],
  address?: Address
}

interface NewOrder {
  idCategory: string;
  addresses: MapAddress[];
}

 const NewOrder:  React.FC<NewOrder> = ({ idCategory, addresses }) => {
  const [showModal, setShowModal] = useState(false);

  const apiUrlCategory = ApiUrl + "category-vehicle-type/";
  const apiUrlJobs = ApiUrl + "jobs/";

  const [categoryVehicleTypes, setCategoryVehicleTypes] = useState([]);

  useEffect(() => {
    fetch(apiUrlCategory).then(async response => {
      setCategoryVehicleTypes(await response.json());
    });
  }, []);

  const [vehicleId, setVehicleId] = useState<number>(0);
  const [commentDriver, setCommentDriver] = useState<string>();
  const [volume, setVolume] = useState<string>();
  const [weight, setWeight] = useState<string>();
  const [dateTime, setdateTime] = useState<string>();
  const [modalReady, setModalReady] = useState(false);

  let history = useHistory();

  return (
    <div className="container-place-order">
      <div>
        <IonRow>
          <IonCol size="1">
            <div className="position-div div-purple"> </div>
          </IonCol>
          <IonCol className="black-text" size="11">
            {addresses.length > 0 ? `${addresses[0].address?.street}, д. ${addresses[0].address?.house}` : null}
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol size="1">
            <div className="position-div div-yellow"> </div>
          </IonCol>
          <IonCol className="black-text" size="11">
            {addresses.length > 1 ? `${addresses[1].address?.street}, д. ${addresses[1].address?.house}` : null}
          </IonCol>
        </IonRow>
        <VehicleTypeSelector
          onTypeClicked={id => setVehicleId(id)}
          activeId={vehicleId}
          vehicleTypes={categoryVehicleTypes.filter((vtype: any) => vtype.category.id == idCategory)} />
        <IonRow>
          <IonCol size="6">
            <IonRow>
              <div className="black-text">
                <img className="payment-image" src="../../assets/icon/visa_PNG3.png" />
              .1234
            </div>
            </IonRow>
          </IonCol>
          <IonCol size="6">
            <div className="wishes black-text">
              <img src="../../assets/icon/plus.png" className="icon" />
              Пожелания
            </div>
          </IonCol>
        </IonRow>
        <section>

          <IonContent>
            <IonModal isOpen={showModal} cssClass='my-custom-class'>
              <IonText className="wish-margin black-text"> <strong>Пожелания {vehicleId}</strong></IonText>
              <IonText className="wish-margin black-text"> Укажите ваши пожелания и дополнительную информацию, если считаете её необходимой</IonText>
              <IonItem>
                <IonInput value={commentDriver} onIonChange={(e) => setCommentDriver((e.target as HTMLInputElement).value)} placeholder="Комментарии водителю" />
              </IonItem>
              <IonItem>
                <IonInput value={volume} onIonChange={(e) => setVolume((e.target as HTMLInputElement).value)} placeholder="Объем м&sup3;" />
              </IonItem>
              <IonItem>
                <IonInput value={weight} onIonChange={(e) => setWeight((e.target as HTMLInputElement).value)} placeholder="Вес кг" />
              </IonItem>
              <IonItem>
                <IonDatetime value={dateTime} onIonChange={(e) => setdateTime((e.target as HTMLInputElement).value)} placeholder="Время и дата" display-timezone="utc" />
              </IonItem>

              <IonButton color="primary" expand="block" className="place-button"
                         disabled={addresses.length === 0}
                         onClick={async () => {

                console.log("req body", vehicleId, commentDriver, volume, weight, dateTime);
                axios.post(apiUrlJobs, {
                  name: commentDriver,//todo нужно поле
                  category: idCategory,
                  description: commentDriver,
                  address: addresses.length > 0 ? addresses[0].address?.id : null,
                  dropoff: addresses.length > 1 ? addresses[1].address?.id : null,
                  quantity: volume,
                  date: dateTime,
                  vehicle_type: vehicleId,
                  status: 1,
                  customer: 1//todo need add id user
                  //weight свободно
                })
                  .then(function (res: any) {
                    console.log("res " + res);
                  })
                  .catch(function (error: any) {
                    console.log(error);
                  });
                setShowModal(false);
                setModalReady(true);
              }}>Готово</IonButton>


              <IonContent>
                <IonModal isOpen={modalReady} cssClass='my-custom-class'>
                  <OrderReady />
                  <IonButton color="primary" expand="block" className="place-button" onClick={() => {
                    history.goBack()
                  }
                  }>Вернуться в меню заказов</IonButton>
                </IonModal>
              </IonContent>

            </IonModal>
          </IonContent>
          <IonButton onClick={() => {
            if (!vehicleId) {
              console.log("Не выбран тип машины (ид)");
              return;
            } else {
              setShowModal(true)
            }
          }
          } color="primary" expand="block" className="place-button">Заказать</IonButton>
        </section>
      </div>
    </div>
  );
};

export default NewOrder;
