import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
  IonModal,
} from '@ionic/react';
import React, {useEffect, useState} from 'react';
import './PlaceOrder.css';

import NewOrder, {MapAddress} from "../../components/NewOrder";
import { RouteComponentProps, useHistory } from "react-router";
import { YMaps, Map, Placemark, withYMaps} from "react-yandex-maps";
import {ApiUrl} from "../../services/ApiService";
import Address from "../../models/Address";

const OurMap = (props: any) => {

  const [addresses, setAddresses] = useState([] as MapAddress[]);

  function onMapClick(event: any) {
    const coords = event.get('coords') as number[];

    if (addresses.length === 2) {
      setAddresses([{ coords }]);
    } else {
      setAddresses([...addresses, { coords }]);
    }
  }

  useEffect(() => {
    if (addresses.length > 0) {
      const [lat, lon] = addresses[addresses.length - 1].coords;
      fetch(`${ApiUrl}addresses/geocode/?lat=${lat}&lon=${lon}`).then(async (response) => {
        const json = await response.json();
        addresses[addresses.length - 1].address = json as Address;
        props.onAddressChange(addresses);
      });
    }
  }, [addresses]);

  return <Map
      width="100%" height="100%" defaultState={{ center: [43.3219485, 45.674585], zoom: 16 }}
      onClick={onMapClick}>
      {addresses.map(({coords}) => (
        <Placemark
          key={coords.join(",")}
          geometry={coords}
        />
      ))}
    </Map>
};

interface PlaceOrderPageProperties extends RouteComponentProps<{
  categoryId: string;
  history:any
}> { }

const PlaceOrderPage: React.FC<PlaceOrderPageProperties> = ({match}: PlaceOrderPageProperties) => {

  const history = useHistory();

  const [addresses, setAddresses] = useState([] as MapAddress[]);

  return (

    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Заказы</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <div className="z">
          <IonButton color="light" className="back-button" onClick={()=> {
                history.goBack()
              }}
           >&lt;</IonButton>
        </div>

        <div className="map-wrapper">
          <YMaps>
            <OurMap onAddressChange={(addresses: any) => {
              setAddresses(addresses);
            }}/>
          </YMaps>
        </div>
              
        <IonModal isOpen={true}>
          <NewOrder addresses={addresses} idCategory={match.params.categoryId} />
        </IonModal>
      </IonContent>
    </IonPage>
  );
};

export default PlaceOrderPage;
