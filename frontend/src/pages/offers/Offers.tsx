import {
  IonButtons, IonContent,
  IonHeader, IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
  IonModal,
  IonAlert
} from '@ionic/react';
import React, { useState } from 'react';
import './Offers.css';
import axios from 'axios';

import OffersList from "../../components/OffersList";
import { RouteComponentProps, Route, useHistory } from "react-router";
import { ApiUrl } from '../../services/ApiService';

interface OfferPageProperties extends RouteComponentProps<{
  jobId: string;
}> { }


const OfferPage: React.FC<OfferPageProperties> = ({ match }: OfferPageProperties) => {
  const ApiUrlOrders=ApiUrl+"orders/"
  const history = useHistory();
  const [showAlert, setShowAlert] = useState(false);
  const [idOffer, setIdOffer] = useState<string>();

  return (

    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Предложения</IonTitle>
        </IonToolbar>
      </IonHeader>


      <IonAlert
        isOpen={showAlert}
        onDidDismiss={() => setShowAlert(false)}
        cssClass='my-custom-class'
        message={'Если вы подтвердите это предложение, остальные предложения будут отклонены'}
        buttons={[
          {
            text: 'Отменить',
            role: 'cancel',
            cssClass: 'secondary',
            handler: blah => {
              console.log('Закрыто');
            }
          },
          {
            text: 'Потвердить',
            handler: () => {
              console.log('Confirm Okay with id offer ' + idOffer);
              axios.post(ApiUrlOrders, {
                status: '1',
                offer: idOffer?.toString()
              })
                .then(function (res: any) {
                  console.log("res " + res);
                })
                .catch(function (error: any) {
                  console.log(error);
                });


              history.push("/orders/");
            }
          }
        ]}
      />

      <IonContent>
        <OffersList idJob={match.params.jobId as any} onSelectClicked={(id) => {
          setShowAlert(true);
          setIdOffer(id)
        }
        }></OffersList>
      </IonContent>
    </IonPage>
  );
};

export default OfferPage;
