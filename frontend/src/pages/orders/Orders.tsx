import {
  IonButtons, IonCard, IonCardContent, IonCardHeader,
  IonContent,
  IonHeader, IonImg, IonItem, IonList,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
  IonGrid,
  IonCol,
  IonRow,
  IonButton,
  IonModal,
  IonAlert,
  IonCardSubtitle,
  IonCardTitle,
  IonPopover
} from '@ionic/react';
import React, { useEffect, useState } from 'react';
import './Orders.css';

import SegmentToggle from "../../components/SegmentToggle";
import CategorySelector from '../../components/CategorySelector';
import Offer from '../../models/Offer';
import { ApiUrl } from '../../services/ApiService';
import Order from '../../models/Order';

const apiUrl = 'http://127.0.0.1:8000/api/jobs';


const Orders: React.FC<any> = ({ history }: any) => {
  const [showModal, setShowModal] = useState(false);
  const segmentLabels = ['Активные', 'Завершенные'];
  const [activeSegment, setActiveSegment] = useState('');
  const onSegmentClicked = (label: string) => {
    setActiveSegment(label);
  };

  const [showAlert1, setShowAlert1] = useState(false);

  const [jobs, setJobs] = useState([] as any[]);

  const [offers, setOffers] = useState<Offer[]>([]);

  const [orders, setOrders] = useState<Order[]>([]);

  useEffect(() => {
    fetch(`${ApiUrl}orders/`).then(async response => {
      setOrders(await response.json());
    });
  }, []);


  useEffect(() => {
    fetch(`${ApiUrl}offers/`).then(async response => {
      setOffers(await response.json());
    });
  }, []);


  useEffect(() => {
    fetch(`${apiUrl}`).then(async response => {
      let orders = (await response.json()).map((order: any) => {
        return { ...order, date: new Date(order.date).toLocaleDateString() }
      });
      setJobs(orders);
    });
  }, [activeSegment]);

  function F(idJob: number) {
    var j = offers.filter(x => x.job == idJob)[0];
    if (j) {
      var or = orders.filter(o => o.offer.toString() == j.id)[0]
      if (or) {
        return (
          <section>
            <IonButton color="primary" expand="block" onClick={() => setShowAlert1(true)} className="details-button">Посмотреть предложение</IonButton>
            <IonPopover
              isOpen={showAlert1}
              cssClass='my-custom-class'
              onDidDismiss={e => setShowAlert1(false)}
            >
              <IonRow>
                <IonCol size="3">
                  <div className="container">
                    <img src={j.vehicle.driver.user.avatar.url} />
                  </div>
                </IonCol>
                <IonCol size="9">
                  <IonRow>
                    <IonCol size="8">
                      <p className="left">{j.vehicle.model.type.name} </p> <p className="left rating"> &#9733;4.7</p>
                    </IonCol>
                    <IonCol size="4">
                      <p className="right">{Math.trunc(j.amount)} &#8381;</p>
                    </IonCol>
                    <IonCol size="8">
                      <p className="left rplate rplate-left">{j.vehicle.registration_plate.substr(0, 6)}</p>
                      <p className="left rplate rplate-right">{j.vehicle.registration_plate.substr(6, 3)}</p>
                    </IonCol>
                    <IonCol size="4">
                      <p className="right">900m</p>
                    </IonCol>
                  </IonRow>
                </IonCol>
                <IonCol>
                  <IonButton onClick={e => setShowAlert1(false)} color="primary" expand="block" className="place-button">Закрыть</IonButton>
                </IonCol>
              </IonRow>
            </IonPopover>
          </section>

        )
      } else {
        return (
          <section>
            <IonButton color="warning" expand="block" onClick={()=>{
              history.push(`/offers/${idJob}`)
            }
            } className="details-button">Принять предложения</IonButton>
          </section>
        )
      }
    } else {
      return (
        <section>
          <IonButton color="primary" expand="block" className="details-button">Нет предложении</IonButton>
        </section>
      )
    }
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Заказы</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>

        <div className="content-wrapper">

          <SegmentToggle segmentLabels={segmentLabels} activeLabel={activeSegment} onSegmentClicked={onSegmentClicked} />

          <IonCol>

            {
              jobs.filter(x=> x.status == (activeSegment=="Завершенные"? 2:0) || x.status==1 ).map(job => (
                <IonCard key={job.id}>
                
                  <IonCardHeader>
                    <IonImg src={`https://static-maps.yandex.ru/1.x/?ll=${job.address.location}&size=450,450&z=12&l=map&pt=${job.address.location},pm2rdl1~${job.dropoff.location},pm2grl99`.replace(/\s/g, '')} />
                  </IonCardHeader>

                  <IonCardContent>
                    <IonList>
                      <IonGrid >
                        <IonRow >
                          <IonCol size="8" >
                            <b className="text-date">
                              {job.date}
                            </b>
                          </IonCol>
                          <IonCol size="3">
                            <b className="text-price">
                              {job.category.unit_price * job.quantity}₽
                        </b>
                          </IonCol>
                        </IonRow>
                      </IonGrid>
                      <IonItem className="text-info">
                        <IonImg className="myicon" src="https://cdn4.iconfinder.com/data/icons/car-silhouettes/1000/sedan-512.png" />
                        {job.category.name}, {job.vehicle_type.name}
                      </IonItem>
                      <IonItem className="text-info">
                        <IonImg className="myiconm" src="https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png" />
                        {job.address.full_address}
                      </IonItem>
                      {
                        job.dropoff &&
                        <IonItem className="text-info">
                          <IonImg className="myiconm" src="https://i.imgur.com/lDfYGot.png" />{job.dropoff.full_address}
                        </IonItem>
                      }
                      {
                        F(job.id)
                      }
                    </IonList>
                  </IonCardContent>
                </IonCard>
              ))}

          </IonCol>

        </div>
        <div>
          <IonModal isOpen={showModal}>
            <IonButton onClick={() => setShowModal(false)} className="add-order-button">
              <p className="order-text">Закрыть</p>
            </IonButton>
            <CategorySelector onItemClicked={categoryId => {
              history.push(`/place-order/${categoryId}`)
            }}
            />
          </IonModal>
          <IonButton className="add-order-button add-order-button-bottom" onClick={() => setShowModal(true)}>
            <img className="icon" src="../../assets/icon/plus.png" />
            <p className="order-text">Создать заказ</p>
          </IonButton>
        </div>
      </IonContent>
    </IonPage>
  );
};



export default Orders;
