from rest_framework import serializers

from core.serializers import AddressSerializer
from drivers.serializers import VehicleTypeSerializer, VehicleSerializer, DriverSerializer
from core.serializers import ImageSerializer
from .models import Job, JobImage, Category, ReviewImage, Order, \
    OrderCancellation, Offer, Review, CategoryVehicleType


class CategorySerializer(serializers.ModelSerializer):
    image = ImageSerializer()

    class Meta:
        model = Category
        fields = '__all__'


class CreateJobSerializer(serializers.ModelSerializer):

    class Meta:
        model = Job
        fields = '__all__'


class JobSerializerPopulated(serializers.ModelSerializer):
    address = AddressSerializer()
    dropoff = AddressSerializer()
    category = CategorySerializer()
    vehicle_type = VehicleTypeSerializer()

    class Meta:
        model = Job
        fields = '__all__'


class JobImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobImage
        fields = '__all__'


class OfferSerializer(serializers.ModelSerializer):
    vehicle = VehicleSerializer()

    class Meta:
        model = Offer
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'


class ReviewImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReviewImage
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class OrderCancellationSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderCancellation
        fields = '__all__'


class CategoryVehicleTypeSerializer(serializers.ModelSerializer):
    vehicle_type = VehicleTypeSerializer()
    category = CategorySerializer()

    class Meta:
        model = CategoryVehicleType
        fields = '__all__'
