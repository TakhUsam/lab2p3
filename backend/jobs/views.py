from rest_framework import viewsets, status, generics
from rest_framework.response import Response
from rest_framework.views import APIView

from jobs.serializers import JobSerializerPopulated, JobImageSerializer, CategorySerializer, \
    ReviewImageSerializer, OrderSerializer, OrderCancellationSerializer, \
    OfferSerializer, ReviewSerializer, CategoryVehicleTypeSerializer, CreateJobSerializer
from .models import Job, JobImage, Category, ReviewImage, Order, OrderCancellation, Offer, Review, CategoryVehicleType


class JobViewSet(APIView):
    serializer_class = JobSerializerPopulated
    queryset = Job.objects.all().order_by('-created_at')

    def get(self, request):
        jobs = Job.objects.all().order_by('-created_at')
        serializer = JobSerializerPopulated(jobs, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = CreateJobSerializer(data=request.data)
        if serializer.is_valid():
            instance = serializer.save()
            response_serializer = JobSerializerPopulated(instance)
            return Response(response_serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class JobDetailViewSet(generics.RetrieveUpdateDestroyAPIView):
    queryset = Job.objects
    serializer_class = JobSerializerPopulated


class JobImageViewSet(viewsets.ModelViewSet):
    queryset = JobImage.objects.all().order_by('-created_at')
    serializer_class = JobImageSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by('-created_at')
    serializer_class = CategorySerializer


class ReviewImageViewSet(viewsets.ModelViewSet):
    queryset = ReviewImage.objects.all().order_by('-created_at')
    serializer_class = ReviewImageSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all().order_by('-created_at')
    serializer_class = OrderSerializer


class OrderCancellationViewSet(viewsets.ModelViewSet):
    queryset = OrderCancellation.objects.all().order_by('-created_at')
    serializer_class = OrderCancellationSerializer


class OfferViewSet(viewsets.ModelViewSet):
    queryset = Offer.objects.all().order_by('-created_at')
    serializer_class = OfferSerializer


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all().order_by('-created_at')
    serializer_class = ReviewSerializer


class CategoryVehicleTypeViewSet(viewsets.ModelViewSet):
    queryset = CategoryVehicleType.objects.all().order_by('-created_at')
    serializer_class = CategoryVehicleTypeSerializer



