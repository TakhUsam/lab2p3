import json

from django.contrib.auth import authenticate
from django.http import HttpResponseNotFound
from rest_framework.authtoken.models import Token
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import User, City, Message, Image, Address
from core.serializers import UserSerializer, CitySerializer, MessageSerializer, ImageSerializer, AddressSerializer
import pi_2017_django.settings as settings

import requests


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all().order_by('-created_at')
    serializer_class = CitySerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all().order_by('-created_at')
    serializer_class = MessageSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Image.objects.all().order_by('-created_at')
    serializer_class = ImageSerializer


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all().order_by('-created_at')
    serializer_class = AddressSerializer


class LoginViewSet(APIView):
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        userLogin = request.data.get("username")
        userPass = request.data.get("password")
        user = authenticate(email=userLogin, password=userPass)
        if user is not None:
            token, created = Token.objects.get_or_create(user=user)
            return Response({
                'token': token.key,
                'user_id': user.pk,
                'email': user.email
            })
        else:
            return HttpResponseNotFound('<h1>User not found</h1>')



class AddressGeocoderViewSet(APIView):

    def get(self, request):
        lon = request.GET['lon']
        lat = request.GET['lat']
        coords = f'{lon}, {lat}'
        count = 1

        url = f'https://geocode-maps.yandex.ru/1.x/?apikey={settings.YANDEX_GEOCODING["API_KEY"]}&geocode={coords}&lang=ru&format=json&results={count}&kind=house'
        try:
            response = requests.get(url)
            address_details = response.json()['response']['GeoObjectCollection']['featureMember'][0] \
                ['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']

            full_address = address_details['AddressLine']
            locality = address_details['AdministrativeArea']['SubAdministrativeArea']['Locality']
            city_name = locality['LocalityName']
            city, _ = City.objects.get_or_create(name=city_name)

            street = locality['Thoroughfare']['ThoroughfareName']
            house = locality['Thoroughfare']['Premise']['PremiseNumber']

            address, _ = Address.objects.get_or_create(house=house,
                                                       street=street,
                                                       city=city,
                                                       full_address=full_address,
                                                       location=coords)

        except Exception as ex:
            raise ex
        return Response(AddressSerializer(address).data)
