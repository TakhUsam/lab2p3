from django.urls import include, path
from rest_framework import routers
from .views import UserViewSet, AddressViewSet, CityViewSet, ImageViewSet, MessageViewSet, AddressGeocoderViewSet, \
    LoginViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'addresses', AddressViewSet)
router.register(r'cities', CityViewSet)
router.register(r'images', ImageViewSet)
router.register(r'messages', MessageViewSet)

urlpatterns = [
    path('addresses/geocode/', AddressGeocoderViewSet.as_view(), name='address_gecode'),
    path("login/", LoginViewSet.as_view(), name="login"),
    path('', include(router.urls)),
]
